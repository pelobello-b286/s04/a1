from django.urls import path

from . import views

app_name = 'todolist'
urlpatterns = [
	# The path() function receive four arguments
	# We'll focus on 2 arguments that are required, 'route' and 'view'
	path('', views.index, name='index'),
		  #endpoint ''

	#todolist/<todoitem_id>
	#The <int:todoitem_id>/ allows for creating a dynamic link where the todoitem_id is provided
	#todoitem/#wildcard endpoint
	path('<int:todoitem_id>/', views.todoitem,name="viewtodoitem"),
	path('<int:eventitem_id>/', views.eventitem,name="vieweventitem"),

	path('register/', views.register_view, name="register"),
	path('change_password/', views.change_password, name="change_password"),
	path('login/', views.login_view, name="login"),
	path('logout/', views.logout_view, name="logout"),
	#addtask route
	path('add_task', views.add_task, name="add_task"),
	path('add_event', views.add_event, name="add_event")
]
