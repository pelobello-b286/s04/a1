from django.contrib import admin

# Register your models here.
from .models import ToDoItem, ToDoEvent

admin.site.register(ToDoItem)
admin.site.register(ToDoEvent)