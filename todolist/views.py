from django.shortcuts import render


from django.http import HttpResponse

#User is builtin nasa django no need to create a db
from django.contrib.auth.models import User 
from django.contrib.auth import authenticate, login, logout
# get object or 404 premade na
from django.shortcuts import render,redirect, get_object_or_404
from django.forms.models import model_to_dict
from django.utils import timezone
#local imports
from .models import ToDoItem, ToDoEvent
from .forms import LoginForm, AddTaskForm, RegisterUser, AddEventForm
# Create your views here.

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	eventitem_list = ToDoEvent.objects.filter(user_id = request.user.id)
	context = {
		"todoitem_list" : todoitem_list,
		"eventitem_list" : eventitem_list,
		"user" : request.user
	}
	return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def eventitem(request, eventitem_id):
	eventitem = get_object_or_404(ToDoEvent, pk=todoitem_id)
	return render(request, "todolist/eventitem.html", model_to_dict(eventitem))

def register_view(request):		
	user = User.objects.all()
	is_user_registered = False
	context = {}

	if request.method == "POST":
		form = RegisterUser(request.POST)
		if form.is_valid() == False:
			form = RegisterUser()
		else:
			for indiv_user in user:
				if indiv_user.username == form.cleaned_data['username']:
					is_user_registered = True
					break;

			if is_user_registered == False:
				user = User() 
				user.username = form.cleaned_data['username']
				user.first_name = form.cleaned_data['first_name']
				user.last_name = form.cleaned_data['last_name']
				user.email = form.cleaned_data['email']
				user.set_password(form.cleaned_data['password'])
				user.is_staff = False
				user.is_active = True
				user.save()
				return redirect("todolist:login")

				context = {
					"first_name" : user.first_name,
					"last_name" : user.last_name
				}
	return render(request, "todolist/register.html", context)


def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="johndoe", password="john1234")
	print(user)

	if user is not None:
		# this is where we get the user with username 'John Doe'
		authenticated_user = User.objects.get(username='johndoe')
		# this is where we change the password
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True
		context = {
			"is_user_authenticated": is_user_authenticated
		}
		return render(request, "todolist/change_password.html", context)

def login_view(request):

	context = {}
	if request.method == "POST":
		form = LoginForm(request.POST)
		if form.is_valid() == False:
			form = LoginForm()
		else:
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			context = {
				"username" : username,
				"password" : password
			}
			if user is not None:
				login(request,user)
				return redirect("todolist:index")
			else:
				context = {
					"error": True
				}
	return render(request, "todolist/login.html", context)


def logout_view(request):
	logout(request)
	#no need for logout.html since naka redirect na sya sa index.html
	return redirect("todolist:index")

def add_task(request):
	context = {}

	if request.method == "POST":
		form = AddTaskForm(request.POST)
		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			duplicate = ToDoItem.objects.filter(task_name = task_name)

			if not duplicate:
				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id = request.user.id)
				return redirect("todolist:index")
			else:
				context= {
					"error" : True
				}
	return render(request, "todolist/add_task.html", context)

def add_event(request):
	context = {}

	if request.method == "POST":
		form = AddEventForm(request.POST)
		if form.is_valid() == False:
			form = AddEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			event_date = form.cleaned_data['event_date']
			duplicate = ToDoEvent.objects.filter(event_name = event_name)

			if not duplicate:
				ToDoEvent.objects.create(event_name=event_name, description=description, event_date=event_date, date_created=timezone.now(), user_id = request.user.id)
				return redirect("todolist:index")
			else:
				context= {
					"error" : True
				}
	return render(request, "todolist/add_event.html", context)




